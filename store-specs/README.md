# eventsource-store-specs

This project provides an open store specification, as a HUnit test, for store implementors.
The goal is to make sure every `Store` implementation behaves the same regarding to those tests.
