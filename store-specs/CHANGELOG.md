1.2.1
=====
    * Require `readStream` to not panic if a non-existing stream is requested.

1.2.0
=====
    * Migrate to `eventsource-api` 1.5.0.
    * Remove Aggregate API specifications.

1.1.1
=====
    * Fix aggregate specifications.

1.1.0
=====
    * Add Aggregate API specifications.

1.0.1
=====
    * Fix Stackage build.
