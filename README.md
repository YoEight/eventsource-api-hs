# Haskell Eventsourcing

Home of an attempt to formalize eventsourcing.

We use [stack][] to build project under this repository.

## How to work on this project ?

### Using [stack][]

```sh
# Build the code
$ stack build

# Test the code
# (GetEventStore store implementation will require a running GetEventStore server on 1113 port)
$ stack test
```

### Using [cabal][] (>= 2)

```sh
# Build the code
$ cabal new-install --only-dependencies
$ cabal new-configure
$ cabal new-build

# Test the code
# (GetEventStore store implementation will require a running GetEventStore server on 1113 port)
$ cabal new-test
```

## About testing

Some packages may need prior configuration in order to run their tests (like a live database server)

[stack]: http://www.haskellstack.com
[cabal]: https://www.haskell.org/cabal/download.html

