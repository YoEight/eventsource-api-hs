# eventsource-geteventstore-store

[GetEventStore][] `Store` implementation. It's based on [eventstore][] driver.

##### Testing

In order to run the tests, you'll need a running [GetEventStore][] server at `localhost` on the `1113` port.

[GetEventStore]: http://eventstore.org
[eventstore]: https://gitlab.com/YoEight/eventstore-hs
