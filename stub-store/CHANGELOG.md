1.1.1
=====
  * `readStream` implementation no longer fails when a stream doesn't exist.

1.1.0
=====
  * Migrate to `eventsource-api` 1.5.0.

1.0.3
=====
  * Fix `ExpectedVersion` logic.

1.0.2
=====
  * Fix Stackage build

1.0.1
=====
  * Support changes in api.
